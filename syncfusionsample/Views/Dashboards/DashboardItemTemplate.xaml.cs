﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace syncfusionsample
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DashboardItemTemplate : DashboardItemTemplateBase
    {
        public DashboardItemTemplate()
        {
            InitializeComponent();
        }
    }
}