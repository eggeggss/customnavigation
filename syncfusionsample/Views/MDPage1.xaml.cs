﻿using syncfusionsample.Common;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace syncfusionsample.Views
{
    public class ListItem
    {
        public bool IsActive { set; get; }
        public String Name { set; get; }
        public String Source { set; get; }
    }

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MDPage1 : MasterDetailPage
    {
        public ObservableCollection<ListItem> List = new ObservableCollection<ListItem>();

        public NavigationPage NavigationView { set; get; }

        public bool IsActive { set; get; }

        public event EventHandler ShowMasterPageAnimateEvent;

        public event EventHandler CloseMasterPageAnimateEvent;

        public MDPage1()
        {
            InitializeComponent();

            this.person1.PName = "luffymchd";
            this.person1.PName2 = "19830";
            this.person1.PName3 = "0919-345-567";
            this.person1.PName4 = "Black";
            this.person1.UrlSource = "http://apphome.ithome.com.tw/uploads/image-upload/52d1767f9554d.png";


            List.Add(new ListItem() { Name = "公 佈 欄", Source = "https://sysycloud.ddns.net/download/icons/1.png", IsActive = false });
            List.Add(new ListItem() { Name = "通 訊 錄", Source = "https://sysycloud.ddns.net/download/icons/2.png", IsActive = false });
            List.Add(new ListItem() { Name = "訊 息 紀 錄", Source = "https://sysycloud.ddns.net/download/icons/3.png", IsActive = false });
            List.Add(new ListItem() { Name = "會 議 室", Source = "https://sysycloud.ddns.net/download/icons/4.png", IsActive = false });
            List.Add(new ListItem() { Name = "報 表", Source = "https://sysycloud.ddns.net/download/icons/5.png", IsActive = false });
            this.listView1.ItemsSource = List;

            listView1.ItemTemplate = new Xamarin.Forms.DataTemplate(
                () =>
                {
                    //每個item都會進來設定一次
                    MasterViewCell viewCell = new MasterViewCell();
                    viewCell.SetBinding(MasterViewCell.NameProperty, "Name");
                    viewCell.SetBinding(MasterViewCell.SourceProperty, "Source");

                    //開抽屜要做的事
                    this.ShowMasterPageAnimateEvent += viewCell.OpenAnimate;
                    //關抽屜要做的事
                    this.CloseMasterPageAnimateEvent += viewCell.CloseAnimate;
                    return viewCell;
                });



            Device.OnPlatform(() =>
            {
                this.masterPage.Padding = new Thickness(0, 60, 0, 0);
                this.person1.PName4 = "Black";
            }, () =>
            {
                this.masterPage.Padding = new Thickness(0, 85, 0, 0);
                this.person1.PName4 = "White";
            }, null);

            NavigationView = new NavigationPage(new WelcomePage1())
            {
                //BarBackgroundColor = Color.FromHex("#1E90FF"),
                BarTextColor = Color.White
                
            };

            //
            //this.Master.Icon = "menu.png";



            // MasterPage mpage= new MasterPage() { Icon = "menu.png" };
            // this.Master = mpage;
            this.Detail = NavigationView;

            this.Detail.ToolbarItems.Add(new ToolbarItem() { Icon = "settings_icon.png" ,Priority=1});


            this.Master.Icon = "menu.png";

            //開關抽屜的事件
            this.IsPresentedChanged += MyMasterDetailPage_IsPresentedChanged;

        }


        private void MyMasterDetailPage_IsPresentedChanged(object sender, EventArgs e)
        {
            if (this.IsPresented == true)
            {
                if (this.ShowMasterPageAnimateEvent != null)
                    this.ShowMasterPageAnimateEvent(sender, e);
            }
            else
            {
                if (this.CloseMasterPageAnimateEvent != null)
                    this.CloseMasterPageAnimateEvent(sender, e);
            }
        }

        private void Master_Appearing(object sender, EventArgs e)
        {

            //DisplayAlert("test","test","ok");
            //throw new NotImplementedException();
        }

        private void listView1_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            listView1.SelectedItem = null;
        }

    }
}