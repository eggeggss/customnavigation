﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using UXDivers.Artina.Shared;
using Prism.Unity;
using syncfusionsample.Views;

namespace syncfusionsample
{
    //[XamlCompilation (XamlCompilationOptions.Skip)]
    public partial class App : PrismApplication
    {

        public App(IPlatformInitializer initializer = null) : base(initializer) { }

        protected override void OnInitialized()
        {
            InitializeComponent();
            NavigationService.NavigateAsync("MDPage1");
            //NavigationService.NavigateAsync("NavigationPage/MainPage?title=Hello%20from%20Xamarin.Forms");
        }

        protected override void RegisterTypes()
        {
            Container.RegisterTypeForNavigation<NavigationPage>();

            //Container.RegisterTypeForNavigation<WelcomePage1>();
            //Container.RegisterTypeForNavigation<MDPage1>();
            Container.RegisterTypeForNavigation<PrismContentPage1>();
            Container.RegisterTypeForNavigation<WelcomePage1>();
            Container.RegisterTypeForNavigation<MDPage1>();
            Container.RegisterTypeForNavigation<CustomCameraPage>();
        }

    }
}
