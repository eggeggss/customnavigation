using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using syncfusionsample.Resx;
using UXDivers.Artina.Shared;
using Prism.Services;
using Prism.Navigation;
using Prism.Events;

namespace syncfusionsample
{
    public  class SamplesDefinition
    {
        

        public  IPageDialogService _dialogService;
        public  INavigationService _navigationService;
        public  IEventAggregator _eventAggregator;
        /*
        public SamplesDefinition(IPageDialogService dialogService, INavigationService navigationService, IEventAggregator eventAggregator)
        {
            _dialogService = dialogService;
            _navigationService = navigationService;
            _eventAggregator = eventAggregator;
        }
        */
        private static CultureChangeNotifier _notifier;

        private static List<SampleCategory> _samplesCategoryList;
        private static List<Sample> _allSamples;
        private static List<SampleGroup> _samplesGroupedByCategory;

        private static string[] _categoriesColors =
        {
            "#921243",
            "#B31250",
            "#CD195E",
            "#56329A",
            "#6A40B9",
            "#7C4ECD",
            "#525ABB",
            "#5F7DD4",
            "#7B96E5"
        };

        public static void Initialize()
        {
            _notifier = new CultureChangeNotifier();
            _notifier.CultureChanged += (sender, args) => InitializeSamples();
        }

        public static List<SampleCategory> SamplesCategoryList
        {
            get
            {
                if (_samplesCategoryList == null)
                {
                    InitializeSamples();
                }

                return _samplesCategoryList;
            }
        }

        public static List<Sample> AllSamples
        {
            get
            {
                if (_allSamples == null)
                {
                    InitializeSamples();
                }

                return _allSamples;
            }
        }

        public static List<SampleGroup> SamplesGroupedByCategory
        {
            get
            {
                if (_samplesGroupedByCategory == null)
                {
                    InitializeSamples();
                }

                return _samplesGroupedByCategory;
            }
        }

        public  List<SampleCategory> CreateSamples()
        {
            var result = new List<SampleCategory>();

            result.Add(
                new SampleCategory
                {
                    //test
                    Id = 0,
                    Name = AppResources.StringCategorySocial,               // "Social",
                    BackgroundColor = Color.FromHex(_categoriesColors[0]),
                    BackgroundImage = SampleData.DashboardImagesList[6],
                    Icon = "https://sysycloud.ddns.net/download/icons/1.png",//"pika_50.png",//GrialShapesFont.CardVisa,
                    Badge = 0,
                    EventHandle = new System.EventHandler(async (s,e)=> {
                        await _navigationService.NavigateAsync("PrismContentPage1");
                        //_dialogService.DisplayAlertAsync("info", "hi", "ok");

                    })
                }
            );

            result.Add(
                new SampleCategory
                {
                    Id = 1,
                    Name = AppResources.StringCategoryArticles,                 // "Articles",
                    BackgroundColor = Color.FromHex(_categoriesColors[1]),
                    BackgroundImage = SampleData.DashboardImagesList[4],
                    Icon = "https://sysycloud.ddns.net/download/icons/2.png",//GrialShapesFont.InsertFile,
                    Badge = 2,
                    EventHandle = new System.EventHandler(async (s, e) => {
                        await _navigationService.NavigateAsync("CustomCameraPage");
                        //_dialogService.DisplayAlertAsync("info", "hi", "ok");

                    })
                }
            );

            result.Add(
                new SampleCategory
                {
                    Id = 2,
                    Name = AppResources.StringCategoryDashboards,               //"Dashboards",
                    BackgroundColor = Color.FromHex(_categoriesColors[2]),
                    BackgroundImage = SampleData.DashboardImagesList[3],
                    Badge = 5,
                    Icon = "https://sysycloud.ddns.net/download/icons/3.png"//GrialShapesFont.Dashboard

                }
            );

            result.Add(
                new SampleCategory
                {
                    Id = 3,
                    Name = AppResources.StringCategoryNavigation,                   // "Navigation",
                    BackgroundColor = Color.FromHex(_categoriesColors[3]),
                    BackgroundImage = SampleData.DashboardImagesList[2],
                    Badge = 5,
                    Icon = "https://sysycloud.ddns.net/download/icons/4.png"//GrialShapesFont.Menu
                }
            );

            result.Add(
                new SampleCategory
                {
                    Id = 4,
                    Name = AppResources.StringCategoryLogins,               // "Logins",
                    BackgroundColor = Color.FromHex(_categoriesColors[4]),
                    BackgroundImage = SampleData.DashboardImagesList[5],
                    Badge = 2,
                    Icon = "https://sysycloud.ddns.net/download/icons/5.png"//GrialShapesFont.Lock
                }
            );

            result.Add(
                new SampleCategory
                {
                    Id = 5,
                    Name = AppResources.StringCategoryECommerce,                // "Ecommerce",
                    BackgroundColor = Color.FromHex(_categoriesColors[5]),
                    BackgroundImage = SampleData.DashboardImagesList[1],
                    Badge = 3,
                    Icon = "https://sysycloud.ddns.net/download/icons/6.png"//GrialShapesFont.ShoppingCart
                }
            );

            result.Add(
                new SampleCategory
                {
                    Id = 6,
                    Name = AppResources.StringCategoryWalkthroughs,             // "Walkthroughs",
                    BackgroundColor = Color.FromHex(_categoriesColors[6]),
                    BackgroundImage = SampleData.DashboardImagesList[7],
                    Badge = 2,
                    Icon = "https://sysycloud.ddns.net/download/icons/7.png"//GrialShapesFont.Carousel
                }
            );

            result.Add(
                new SampleCategory
                {
                    Id = 7,
                    Name = AppResources.StringCategoryMessages,             // "Messages",
                    BackgroundColor = Color.FromHex(_categoriesColors[7]),
                    BackgroundImage = SampleData.DashboardImagesList[8],
                    Badge = 2,
                    Icon = "https://sysycloud.ddns.net/download/icons/8.png"//GrialShapesFont.Email
                }
            );

            result.Add(
                new SampleCategory
                {
                    Id = 8,
                    Name = AppResources.StringCategoryTheme,                        // "Grial Theme",
                    BackgroundColor = Color.FromHex(_categoriesColors[8]),
                    BackgroundImage = SampleData.DashboardImagesList[0],
                    Badge = 9,
                    Icon = "https://sysycloud.ddns.net/download/icons/9.png"//GrialShapesFont.ColorPalette
                }
            );

            result.Add(
                new SampleCategory
                {
                    Id = 9,
                    Name = AppResources.StringCategoryTheme,                        // "Grial Theme",
                    BackgroundColor = Color.FromHex(_categoriesColors[8]),
                    BackgroundImage = SampleData.DashboardImagesList[0],
                    Badge = 9,
                    Icon = "https://sysycloud.ddns.net/download/icons/10.png"//GrialShapesFont.ColorPalette
                }
            );

            result.Add(
                 new SampleCategory
                 {
                     Id = 10,
                     Name = AppResources.StringCategoryTheme,                       // "Grial Theme",
                     BackgroundColor = Color.FromHex(_categoriesColors[8]),
                     BackgroundImage = SampleData.DashboardImagesList[0],
                     Badge = 9,
                     Icon = "https://sysycloud.ddns.net/download/icons/11.png"//GrialShapesFont.ColorPalette
                 }
             );

            result.Add(
                 new SampleCategory
                 {
                     Id = 11,
                     Name = AppResources.StringCategoryTheme,                       // "Grial Theme",
                     BackgroundColor = Color.FromHex(_categoriesColors[8]),
                     BackgroundImage = SampleData.DashboardImagesList[0],
                     Badge = 9,
                     Icon = "https://sysycloud.ddns.net/download/icons/12.png"//GrialShapesFont.ColorPalette
                 }
             );

            result.Add(
                new SampleCategory
                {
                    Id = 12,
                    Name = AppResources.StringCategoryTheme,                       // "Grial Theme",
                    BackgroundColor = Color.FromHex(_categoriesColors[8]),
                    BackgroundImage = SampleData.DashboardImagesList[0],
                    Badge = 9,
                    Icon = "https://sysycloud.ddns.net/download/icons/13.png"//GrialShapesFont.ColorPalette
                }
            );

            result.Add(
                            new SampleCategory
                            {
                                Id = 13,
                                Name = AppResources.StringCategoryTheme,                       // "Grial Theme",
                                BackgroundColor = Color.FromHex(_categoriesColors[8]),
                                BackgroundImage = SampleData.DashboardImagesList[0],
                                Badge = 9,
                                Icon = "https://sysycloud.ddns.net/download/icons/14.png"//GrialShapesFont.ColorPalette
                            }
                        );
            result.Add(
                            new SampleCategory
                            {
                                Id = 14,
                                Name = AppResources.StringCategoryTheme,                       // "Grial Theme",
                                BackgroundColor = Color.FromHex(_categoriesColors[8]),
                                BackgroundImage = SampleData.DashboardImagesList[0],
                                Badge = 9,
                                Icon = "https://sysycloud.ddns.net/download/icons/15.png"//GrialShapesFont.ColorPalette
                            }
                        );


            return result;
        }

        public static void InitializeSamples()
        {
            try
            {
                var cate = new SamplesDefinition();

                _samplesCategoryList = cate.CreateSamples();

                _allSamples = new List<Sample>();

                _samplesGroupedByCategory = new List<SampleGroup>();
                
            }
            catch (System.Exception ex)
            {
                throw new System.Exception($"{ex.Message}");
            }
        }
    }

    public class SampleGroup : List<Sample>
    {
        public SampleGroup(string name)
        {
            Name = name;
        }

        public string Name { get; }
    }
}
