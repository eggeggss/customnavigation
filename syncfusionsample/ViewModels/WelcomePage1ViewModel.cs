﻿using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace syncfusionsample.ViewModels
{

    public class WelcomePage1ViewModel : BindableBase, INavigationAware
    {
        #region Repositories (遠端或本地資料存取)

        #endregion

        #region ViewModel Property (用於在 View 中作為綁定之用)

        #region MyText
        private string _MyText;
        /// <summary>
        /// PropertyDescription
        /// </summary>
        public string MyText
        {
            get { return this._MyText; }
            set { this.SetProperty(ref this._MyText, value); }
        }
        #endregion


        #region Items
        private List<SampleCategory> _Items;
        /// <summary>
        /// Items
        /// </summary>
        public List<SampleCategory> Items
        {
            get { return this._Items; }
            set { this.SetProperty(ref this._Items, value); }
        }
        #endregion


        #endregion

        #region Field 欄位

        public readonly IPageDialogService _dialogService;
        private readonly INavigationService _navigationService;
        private readonly IEventAggregator _eventAggregator;
        #endregion

        #region Constructor 建構式
        public WelcomePage1ViewModel(INavigationService navigationService, IEventAggregator eventAggregator,
            IPageDialogService dialogService)
        {

            #region 相依性服務注入的物件

            _dialogService = dialogService;
            _eventAggregator = eventAggregator;
            _navigationService = navigationService;
            #endregion

            #region 頁面中綁定的命令

            #endregion
            //this.Items = SamplesDefinition.SamplesCategoryList;

            SamplesDefinition sap = new SamplesDefinition();
            sap._dialogService = dialogService;
            sap._navigationService = navigationService;
            sap._eventAggregator = eventAggregator;

            this.Items = sap.CreateSamples();//SamplesDefinition.SamplesCategoryList;

            #region 事件聚合器訂閱
            this.MyText = "Hello";
            #endregion
        }

        #endregion

        #region Navigation Events (頁面導航事件)
        public void OnNavigatedFrom(NavigationParameters parameters)
        {

        }

        public async void OnNavigatedTo(NavigationParameters parameters)
        {
            await ViewModelInit();
        }

        public void OnNavigatingTo(NavigationParameters parameters)
        {

        }
        #endregion

        #region 設計時期或者執行時期的ViewModel初始化
        #endregion

        #region 相關事件
        #endregion

        #region 相關的Command定義
        #endregion

        #region 其他方法

        /// <summary>
        /// ViewModel 資料初始化
        /// </summary>
        /// <returns></returns>
        private async Task ViewModelInit()
        {
            await Task.Delay(100);
        }
        #endregion

    }

}
