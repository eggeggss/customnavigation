using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace syncfusionsample
{
	public class SampleCategory
	{
		public int Id { get; set; }

		public string Name { get; set; }

		public Color BackgroundColor { get; set; }

		public String BackgroundImage { get; set; }

		public List<Sample> SamplesList { get; set; }

		public ImageSource Icon { get; set; }
	
		public int Badge { get; set; }

        public EventHandler EventHandle { set; get; }
	}
}