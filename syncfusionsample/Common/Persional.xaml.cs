﻿
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace syncfusionsample
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Persional : ContentView
    {

        public static readonly BindableProperty NameProperty =
        BindableProperty.Create("PName", typeof(string), typeof(Persional), "");
        public string PName
        {
            get
            {
                return (string)GetValue(NameProperty);
            }
            set
            {
                SetValue(NameProperty, value);
                this.lbName.Text = $"Name:{PName}";
            }
        }

        public static readonly BindableProperty Name2Property =
       BindableProperty.Create("PName2", typeof(string), typeof(Persional), "");

        public string PName2
        {
            get
            {
                return (string)GetValue(Name2Property);
            }
            set
            {
                SetValue(Name2Property, value);
                this.lbName2.Text = $"EmpNo:{PName2}";
            }
        }

        public static readonly BindableProperty Name3Property =
      BindableProperty.Create("PName3", typeof(string), typeof(Persional), "");

        public string PName3
        {
            get
            {
                return (string)GetValue(Name3Property);
            }
            set
            {
                SetValue(Name3Property, value);
                this.lbName3.Text = $"Tel:{PName3}";
            }
        }

        public static readonly BindableProperty Name4Property =
      BindableProperty.Create("PName4", typeof(string), typeof(Persional), "");

        public string PName4
        {
            get
            {
                return (string)GetValue(Name4Property);
            }
            set
            {
                SetValue(Name4Property, value);

                if (PName4 == "Black")
                {
                    this.lbName.TextColor = Color.Black;
                    this.lbName2.TextColor = Color.Black;
                    this.lbName3.TextColor = Color.Black;

                }
                else
                {
                    this.lbName.TextColor = Color.White;
                    this.lbName2.TextColor = Color.White;
                    this.lbName3.TextColor = Color.White;

                }

            }
        }


        public static readonly BindableProperty UrlSourceProperty =
      BindableProperty.Create("UrlSource", typeof(string), typeof(Persional), "");

        public string UrlSource
        {
            get
            {
                return (string)GetValue(UrlSourceProperty);
            }
            set
            {
                SetValue(UrlSourceProperty, value);
                this.image1.Source = UrlSource;
            }
        }
        public Persional()
        {
            InitializeComponent();
        }
    }
}