﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace syncfusionsample.Common
{
    public class MasterViewCell : ViewCell
    {

        public static readonly BindableProperty NameProperty =
    BindableProperty.Create("Name", typeof(string), typeof(MasterViewCell), "");

        public string Name
        {
            get
            {
                return (string)GetValue(NameProperty);
            }
            set
            {
                SetValue(NameProperty, value);
                this.CellLable.Text = Name;
            }
        }


        public static readonly BindableProperty SourceProperty =
            BindableProperty.Create("Source", typeof(string), typeof(MasterViewCell), "");

        public string Source
        {
            get
            {
                return (String)GetValue(SourceProperty);
            }
            set
            {
                SetValue(SourceProperty, value);
                this.CellImage.Source = Source;
            }
        }
        

        public Image CellImage { set; get; }
        public Label CellLable { set; get; }

        public MasterViewCell()
        {
            Grid grid = new Grid();
            grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
            grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(4, GridUnitType.Star) });

            CellImage = new Image()
            {
                Aspect = Aspect.AspectFit,
                WidthRequest = 40,
                HeightRequest = 40,
                HorizontalOptions = LayoutOptions.Start
            };

            CellImage.SetBinding(Image.SourceProperty, "Source");

            CellLable = new Label()
            {
                FontAttributes = FontAttributes.Bold,
                FontSize = 16,
                HorizontalOptions = LayoutOptions.Start,
                VerticalOptions = LayoutOptions.CenterAndExpand
            };
            
            Device.OnPlatform(() => {
                CellLable.TextColor = Color.Black; }, 
                () => { CellLable.TextColor = Color.White; }, 
                () => { CellLable.TextColor = Color.Black; });
            
            CellLable.SetBinding(Label.TextProperty, "Name");

            CellImage.Opacity = 0;
            CellLable.Opacity = 0;

            grid.Children.Add(CellImage, 0, 0);
            grid.Children.Add(CellLable, 1, 0);
            View = grid;

        }


        //動畫
        public async void OpenAnimate(object sender, EventArgs e)
        {


            await Task.WhenAll(
                  CellImage.FadeTo(1, 500),
                  CellLable.FadeTo(1, 500)
            // CommonView.Animate.Animate.BallAnimate(CellImage, 3, 3, 1)
            );


            //await CommonView.Animate.Animate.BallAnimate(CellImage, 3, 3, 1);


        }

        public void CloseAnimate(object sender, EventArgs e)
        {


            ViewExtensions.CancelAnimations(CellImage);

            CellImage.Opacity = 0;
            CellLable.Opacity = 0;
        }

    }

}
