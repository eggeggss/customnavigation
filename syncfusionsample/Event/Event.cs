﻿using Prism.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace syncfusionsample
{

    public class CameraObject
    {
        public String OcrResult { set; get; }
        public byte[] Imagebyte { set; get; }
    }

    public class CameraEvent : PubSubEvent<CameraObject>
    {

    }

}
