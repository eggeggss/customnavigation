﻿using Newtonsoft.Json;
using syncfusionsample.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace syncfusionsample
{
    public class HttpMethod
    {
     
        public static  Tuple<String, bool> ParseOpenalpr(byte[] image)
        {
            try
            {
                var result = Ocr(image);
                Tuple<String, bool> tuple = null;

                var plate = JsonConvert.DeserializeObject<PlateRoot>(result);

                if (plate.results.Length > 0)
                {
                    var platchildstr = plate.results[0].ToString();

                    var plateresult = JsonConvert.DeserializeObject<PlatChild>(platchildstr);

                    result = plateresult.plate;
                    tuple = new Tuple<string, bool>(result, true);
                }
                else
                {

                    tuple = new Tuple<string, bool>("", false);
                }
                return tuple;
            }
            catch (Exception ex)
            {
                throw new Exception($"ParseOpenalpr fail:{ex.Message}");
            }
            
        }


        public static string Ocr(byte[] image)
        {
            try
            {
                using (var client = new HttpClient())
                {

                    //using (var content =new MultipartFormDataContent("Upload----" + DateTime.Now.ToString(CultureInfo.InvariantCulture)))
                    using (var content = new MultipartFormDataContent("---MyBoundaryXamarin"))
                    {
                        content.Add(new StreamContent(new MemoryStream(image)), "image", "upload.jpg");

                        var message = client.PostAsync("https://api.openalpr.com/v2/recognize?recognize_vehicle=1&country=oz&secret_key=sk_6bce56f6f3d4c643dee16cdf", content).Result;
                        //var message = await client.PostAsync("https://api.openalpr.com/v2/recognize?recognize_vehicle=1&country=oz&secret_key=sk_6bce56f6f3d4c643dee16cdf", content);

                        //var input = await message.Content.ReadAsByteArrayAsync();
                        var input = message.Content.ReadAsByteArrayAsync().Result;


                        var result = Encoding.GetEncoding("us-ascii").GetString(input, 0, input.Length);

                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Call ocr fail :{ex.Message}");
            }
        }

    }

    

}
