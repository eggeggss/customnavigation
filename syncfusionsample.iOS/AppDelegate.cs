using Foundation;
using UIKit;
using Xamarin.Forms;

using Lottie.Forms.iOS.Renderers;

using FFImageLoading.Forms.Touch;

using UXDivers.Artina.Shared;
using System;
using Prism.Unity;
using Microsoft.Practices.Unity;
using Prism.Events;

namespace syncfusionsample.iOS
{
	// The UIApplicationDelegate for the application. This class is responsible for launching the
	// User Interface of the application, as well as listening (and optionally responding) to application events from iOS.
	[Register("AppDelegate")]
	public class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
        public IEventAggregator EventAggregator { set; get; }

        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
		{
			global::Xamarin.Forms.Forms.Init();
			CachedImageRenderer.Init(); // Initializing FFImageLoading
			AnimationViewRenderer.Init(); // Initializing Lottie

			UXDivers.Artina.Shared.GrialKit.Init(new ThemeColors(), "syncfusionsample.iOS.GrialLicense");

			// Code for starting up the Xamarin Test Cloud Agent
#if ENABLE_TEST_CLOUD
			Xamarin.Calabash.Start();
#endif

			FormsHelper.ForceLoadingAssemblyContainingType(typeof(UXDivers.Effects.Effects));
			FormsHelper.ForceLoadingAssemblyContainingType<UXDivers.Effects.iOS.CircleEffect>();

            //LoadApplication (new App ());
            LoadApplication(new App(new iOSInitializer()));

            EventAggregator = ((syncfusionsample.App.Current as PrismApplication).Container as IUnityContainer).Resolve<IEventAggregator>();



            return base.FinishedLaunching(app, options);
		}
	}



    public class iOSInitializer : IPlatformInitializer
    {
        public void RegisterTypes(IUnityContainer container)
        {

        }
    }
}