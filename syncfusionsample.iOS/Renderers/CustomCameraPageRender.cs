﻿using AssetsLibrary;
using AVFoundation;
using CoreGraphics;
using Foundation;
using GPUImage.Filters.ColorProcessing;
using GPUImage.Filters.ImageProcessing;
using GPUImage.Sources;
using MapKit;
using Microsoft.ProjectOxford.Vision;
using Microsoft.ProjectOxford.Vision.Contract;
using syncfusionsample;
using syncfusionsample.iOS;
using syncfusionsample.Views;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesseract.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;


[assembly: ExportRenderer(typeof(CustomCameraPage), typeof(CustomCameraPageRender))]
namespace syncfusionsample
{
    public class CustomCameraPageRender : PageRenderer
    {
        AVCaptureSession captureSession;
        AVCaptureDeviceInput captureDeviceInput;
        AVCaptureStillImageOutput stillImageOutput;
        UIView liveCameraStream;
        UIActivityIndicatorView actitivy;
        UIVisualEffectView effectview;
        //UIView Waitview;
        UIView top;
        UIView bottom, left, right;
        UIButton takePhotoButton;
        UIButton toggleCameraButton;
        UIButton toggleFlashButton;
        UILabel txtlabel;

        protected override void OnElementChanged(VisualElementChangedEventArgs e)
        {
            base.OnElementChanged(e);


            if (e.OldElement != null || Element == null)
            {
                return;
            }

            try
            {
                SetupUserInterface();
                SetupEventHandlers();
                SetupLiveCameraStream();
                AuthorizeCameraUse();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(@"			ERROR: ", ex.Message);
            }

        }

        void SetupUserInterface()
        {
            var centerButtonX = View.Bounds.GetMidX() - 35f;
            var topLeftX = View.Bounds.X + 25;
            var topRightX = View.Bounds.Right - 65;
            var bottomButtonY = View.Bounds.Bottom - 150;
            var topButtonY = View.Bounds.Top + 15;
            var buttonWidth = 70;
            var buttonHeight = 70;

            liveCameraStream = new UIView()
            {
                Frame = new CGRect(0f, 0f, View.Bounds.Width, View.Bounds.Height),
                BackgroundColor = UIColor.Black
            };

            top = new UIView()
            {

                Frame = new CGRect(10, 50, View.Bounds.Width - 20, 5),
                BackgroundColor = UIColor.Green
            };

            left = new UIView()
            {

                Frame = new CGRect(10, 50, 5, 150),
                BackgroundColor = UIColor.Green
            };

            right = new UIView()
            {

                Frame = new CGRect(View.Bounds.Width - 10, 50, 5, 150),
                BackgroundColor = UIColor.Green
            };


            bottom = new UIView()
            {

                Frame = new CGRect(10, 200, View.Bounds.Width - 10, 5),
                BackgroundColor = UIColor.Green
            };

            

            txtlabel = new UILabel
            {

                Frame = new CGRect(10, 220, View.Bounds.Width - 20, 50),
                Text = "請對準車牌並按鈕",
                TextAlignment = UITextAlignment.Center,
                TextColor=UIColor.Red,
                Font=UIFont.FromName("Helvetica-Bold", 20f)


             };
            
            UIVisualEffect blurEffect = UIBlurEffect.FromStyle(UIBlurEffectStyle.Light);

            effectview = new UIVisualEffectView(blurEffect);
            effectview.Frame = View.Bounds;
            effectview.AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight;
            effectview.Hidden = true;
            
            
            actitivy = new UIActivityIndicatorView(UIActivityIndicatorViewStyle.WhiteLarge)
            {
                Center= effectview.Center,
                Hidden=true
            };



            takePhotoButton = new UIButton()
            {
                Frame = new CGRect(centerButtonX, bottomButtonY, buttonWidth, buttonHeight)
            };
            takePhotoButton.SetBackgroundImage(UIImage.FromFile("TakePhotoButton.png"), UIControlState.Normal);

            toggleCameraButton = new UIButton()
            {
                Frame = new CGRect(topRightX, topButtonY + 5, 35, 26)
            };
            toggleCameraButton.SetBackgroundImage(UIImage.FromFile("ToggleCameraButton.png"), UIControlState.Normal);

            toggleFlashButton = new UIButton()
            {
                Frame = new CGRect(topLeftX, topButtonY, 37, 37)
            };
            toggleFlashButton.SetBackgroundImage(UIImage.FromFile("NoFlashButton.png"), UIControlState.Normal);
            

            
            View.Add(liveCameraStream);
            View.Add(top);
            View.Add(left);
            
            
            View.Add(right);
            View.Add(bottom);


            View.Add(takePhotoButton);
            View.Add(toggleCameraButton);
            View.Add(toggleFlashButton);


            View.Add(effectview);
            View.Add(actitivy);
            View.Add(txtlabel);

        }

        void SetupEventHandlers()
        {
            takePhotoButton.TouchUpInside += (object sender, EventArgs e) =>
            {
                CapturePhoto();
            };

            toggleCameraButton.TouchUpInside += (object sender, EventArgs e) =>
            {
                ToggleFrontBackCamera();
            };

            toggleFlashButton.TouchUpInside += (object sender, EventArgs e) =>
            {
                ToggleFlash();
            };
        }

        UIImage cropImage(UIImage srcImage, RectangleF rect)
        {
            using (CGImage cr = srcImage.CGImage.WithImageInRect(rect))
            {
                UIImage cropped = UIImage.FromImage(cr);
                return cropped;
            }
        }
        private UIImage CropImage(UIImage sourceImage, int crop_x, int crop_y, int width, int height)
        {
            var imgSize = sourceImage.Size;
            UIGraphics.BeginImageContext(new SizeF(width, height));
            var context = UIGraphics.GetCurrentContext();
            var clippedRect = new RectangleF(0, 0, width, height);
            context.ClipToRect(clippedRect);
            var drawRect = new RectangleF(-crop_x, -crop_y, (int)imgSize.Width, (int)imgSize.Height);
            sourceImage.Draw(drawRect);
            var modifiedImage = UIGraphics.GetImageFromCurrentImageContext();
            UIGraphics.EndImageContext();
            return modifiedImage;
        }
#region adjust pic
        public static String ReconText(Stream stream)
        {
            try
            {
                VisualFeature[] visualFeatures = new VisualFeature[] 
                { VisualFeature.Adult, VisualFeature.Categories, VisualFeature.Color, VisualFeature.Description, VisualFeature.Faces, VisualFeature.ImageType, VisualFeature.Tags };

                VisionServiceClient vsclient = new VisionServiceClient("5d0c286cb8514cc1a3299a2d0551d842",
                    "https://southeastasia.api.cognitive.microsoft.com/vision/v1.0");
                
                
                var result = vsclient.RecognizeTextAsync(stream, LanguageCodes.English, false).Result;

                StringBuilder sb = new StringBuilder();

                foreach (var Region in result.Regions)
                {
                    foreach (var line in Region.Lines)
                    {
                        sb.AppendLine();
                        //Console.WriteLine();
                        foreach (var work in line.Words)
                        {
                            sb.Append(work.Text);
                            //Console.Write(work.Text);
                            ;
                        }
                    }
                }
                // File.WriteAllText(@"d:\vision.txt", sb.ToString());
                return sb.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            //var result = vsclient.RecognizeTextAsync(imageFileStream, LanguageCodes.ChineseTraditional, false).Result;
        }

        UIImage Sharpness(UIImage photo)
        {

            GPUImagePicture pic = new GPUImagePicture(photo);
            GPUImageSharpenFilter filter = new GPUImageSharpenFilter();
            filter.Sharpness = 10.0f;
            filter.ForceProcessingAtSize(photo.Size);
            pic.AddTarget(filter);
            pic.UseNextFrameForImageCapture();
            pic.ProcessImage();
            photo = filter.CreateFilteredImage(photo);
            return photo;
        }

        UIImage Tholdess(UIImage photo)
        {
            GPUImagePicture pic = new GPUImagePicture(photo);
            GPUImageAdaptiveThresholdFilter filter = new GPUImageAdaptiveThresholdFilter();
            //filter.BlurRadiusInPixels = 3.0f;
            filter.BlurRadiusInPixels = 10.0f;
            pic.AddTarget(filter);
            pic.UseNextFrameForImageCapture();
            pic.ProcessImage();
            photo = filter.CreateFilteredImage(photo);
            return photo;
        }
        UIImage Grayness(UIImage photo)
        {
            GPUImagePicture pic = new GPUImagePicture(photo);
            GPUImageGrayscaleFilter filter = new GPUImageGrayscaleFilter();
            
            pic.AddTarget(filter);
            pic.UseNextFrameForImageCapture();
            pic.ProcessImage();
            photo = filter.CreateFilteredImage(photo);
            return photo;
        }
        UIImage Contrastness(UIImage photo)
        {
            GPUImagePicture pic = new GPUImagePicture(photo);
            GPUImageContrastFilter filter = new GPUImageContrastFilter();
            filter.Contrast=3.0f;
            pic.AddTarget(filter);
            pic.UseNextFrameForImageCapture();
            pic.ProcessImage();
            photo = filter.CreateFilteredImage(photo);
            return photo;
        }

        UIImage Brightness(UIImage photo)
        {
            GPUImagePicture pic = new GPUImagePicture(photo);
            
            GPUImageBrightnessFilter filter = new GPUImageBrightnessFilter();
            filter.Brightness = 0.9f;
            pic.AddTarget(filter);
            pic.UseNextFrameForImageCapture();
            pic.ProcessImage();
            photo = filter.CreateFilteredImage(photo);
            return photo;
        }
#endregion



        async void CapturePhoto()
        {
            try
            {
                effectview.Hidden = false;
                actitivy.StartAnimating();
                this.txtlabel.Text = "車牌辨識中請稍候";

                var appdelete = (AppDelegate)UIApplication.SharedApplication.Delegate;

                var videoConnection = stillImageOutput.ConnectionFromMediaType(AVMediaType.Video);
                //var videoConnection = stillImageOutput.Connections[0];

                var sampleBuffer = await stillImageOutput.CaptureStillImageTaskAsync(videoConnection);

                var jpegImage = AVCaptureStillImageOutput.JpegStillToNSData(sampleBuffer);

                var photo = new UIImage(jpegImage);
                
                photo = CropImage(photo, 10, 100, (int)photo.Size.Width, 500);



                using (NSData imageData = photo.AsJPEG())
                {
                    Byte[] myByteArray = new Byte[imageData.Length];
                    System.Runtime.InteropServices.Marshal.Copy(imageData.Bytes, myByteArray, 0, Convert.ToInt32(imageData.Length));

                    var tupleresult= HttpMethod.ParseOpenalpr(myByteArray);

                    appdelete.EventAggregator.GetEvent<CameraEvent>().Publish(
                       new CameraObject
                       {
                           Imagebyte = myByteArray,
                           OcrResult = (tupleresult.Item2)?tupleresult.Item1:"please wait a minutes"

                       }
                   );

                }


                effectview.Hidden = true;
                actitivy.StopAnimating();
                actitivy.HidesWhenStopped=true;
                this.txtlabel.Text = "請對準車牌並按鈕";
                
                #region old method
                /*
                photo = Tholdess(photo);
                
                var appdelete = (AppDelegate)UIApplication.SharedApplication.Delegate;
                
                TesseractApi api = new TesseractApi();

                var init = await api.Init("eng");

                api.SetPageSegmentationMode(Tesseract.PageSegmentationMode.Auto);

                api.SetWhitelist("ABCDEFGHIJKLMNOPQRSTVWXYZ0123456789");

                using (NSData imageData = photo.AsJPEG())
                {
                    Byte[] myByteArray = new Byte[imageData.Length];
                    System.Runtime.InteropServices.Marshal.Copy(imageData.Bytes, myByteArray, 0, Convert.ToInt32(imageData.Length));

                    await api.SetImage(myByteArray);

                    var result = api.Text;

                    var result2=ReconText(new MemoryStream(myByteArray));

                    appdelete.EventAggregator.GetEvent<CameraEvent>().Publish(
                        new CameraObject
                        {
                            Imagebyte = myByteArray,
                            OcrResult = "Tesseract=>"+result + "/ microsoft=>" + result2

                        }
                    );

                    System.Diagnostics.Debug.WriteLine($"result:{result}");

                }
                */
                #endregion

                /*
                photo.SaveToPhotosAlbum((image, error) =>
                {
                    Console.Error.WriteLine(@"				Error: ", error);
                });
                */

            }
            catch (Exception ex)
            {
                throw new Exception($"Capture fail:{ex.Message}");
            }
        }

        void ToggleFrontBackCamera()
        {
            var devicePosition = captureDeviceInput.Device.Position;
            if (devicePosition == AVCaptureDevicePosition.Front)
            {
                devicePosition = AVCaptureDevicePosition.Back;
            }
            else
            {
                devicePosition = AVCaptureDevicePosition.Front;
            }

            var device = GetCameraForOrientation(devicePosition);
            ConfigureCameraForDevice(device);

            captureSession.BeginConfiguration();
            captureSession.RemoveInput(captureDeviceInput);
            captureDeviceInput = AVCaptureDeviceInput.FromDevice(device);
            captureSession.AddInput(captureDeviceInput);
            captureSession.CommitConfiguration();
        }

        void ToggleFlash()
        {
            var device = captureDeviceInput.Device;

            var error = new NSError();
            if (device.HasFlash)
            {
                if (device.FlashMode == AVCaptureFlashMode.On)
                {
                    device.LockForConfiguration(out error);
                    device.FlashMode = AVCaptureFlashMode.Off;
                    device.UnlockForConfiguration();
                    toggleFlashButton.SetBackgroundImage(UIImage.FromFile("NoFlashButton.png"), UIControlState.Normal);
                }
                else
                {
                    device.LockForConfiguration(out error);
                    device.FlashMode = AVCaptureFlashMode.On;
                    device.UnlockForConfiguration();
                    toggleFlashButton.SetBackgroundImage(UIImage.FromFile("FlashButton.png"), UIControlState.Normal);
                }
            }
        }

        AVCaptureDevice GetCameraForOrientation(AVCaptureDevicePosition orientation)
        {
            var devices = AVCaptureDevice.DevicesWithMediaType(AVMediaType.Video);

            foreach (var device in devices)
            {
                if (device.Position == orientation)
                {
                    return device;
                }
            }
            return null;
        }

        void SetupLiveCameraStream()
        {
            captureSession = new AVCaptureSession();

            var viewLayer = liveCameraStream.Layer;
            var videoPreviewLayer = new AVCaptureVideoPreviewLayer(captureSession)
            {
                Frame = liveCameraStream.Bounds
            };
            liveCameraStream.Layer.AddSublayer(videoPreviewLayer);

            var captureDevice = AVCaptureDevice.DefaultDeviceWithMediaType(AVMediaType.Video);
            ConfigureCameraForDevice(captureDevice);
            captureDeviceInput = AVCaptureDeviceInput.FromDevice(captureDevice);

            var dictionary = new NSMutableDictionary();
            dictionary[AVVideo.CodecKey] = new NSNumber((int)AVVideoCodec.JPEG);
            stillImageOutput = new AVCaptureStillImageOutput()
            {
                OutputSettings = new NSDictionary()
            };

            captureSession.AddOutput(stillImageOutput);
            captureSession.AddInput(captureDeviceInput);
            captureSession.StartRunning();
        }

        void ConfigureCameraForDevice(AVCaptureDevice device)
        {
            var error = new NSError();
            if (device.IsFocusModeSupported(AVCaptureFocusMode.ContinuousAutoFocus))
            {
                device.LockForConfiguration(out error);
                device.FocusMode = AVCaptureFocusMode.ContinuousAutoFocus;
                device.UnlockForConfiguration();
            }
            else if (device.IsExposureModeSupported(AVCaptureExposureMode.ContinuousAutoExposure))
            {
                device.LockForConfiguration(out error);
                device.ExposureMode = AVCaptureExposureMode.ContinuousAutoExposure;
                device.UnlockForConfiguration();
            }
            else if (device.IsWhiteBalanceModeSupported(AVCaptureWhiteBalanceMode.ContinuousAutoWhiteBalance))
            {
                device.LockForConfiguration(out error);
                device.WhiteBalanceMode = AVCaptureWhiteBalanceMode.ContinuousAutoWhiteBalance;
                device.UnlockForConfiguration();
            }
        }

        async void AuthorizeCameraUse()
        {
            var authorizationStatus = AVCaptureDevice.GetAuthorizationStatus(AVMediaType.Video);
            if (authorizationStatus != AVAuthorizationStatus.Authorized)
            {
                await AVCaptureDevice.RequestAccessForMediaTypeAsync(AVMediaType.Video);
            }
        }

    }


}
